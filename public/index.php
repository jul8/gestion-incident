<?php

require('../src/init.php');

session_start();

use BugApp\Controllers\bugController;
use BugApp\Controllers\UserController;

//var_dump($uri);

switch(true) {

    case preg_match('#^bug/show/(\d+)$#', $uri, $matches):

        $id = $matches[1];

        $controller = new bugController();

        return $controller->show($id);

        break;
    
    case preg_match('#^bug((\?)|$)#', $uri):

        $controller = new bugController();

        return $controller->index();

        break;
    
    case ($uri == 'bug/add'):

        $controller = new bugController();
        
        return $controller->add();
        
        break;
    

    case ($uri == 'login'):
        $controller = new userController();
        return $controller->login();
        break;
        
    case ($uri == 'logout'):
        $controller = new userController();
        return $controller->logout();
        break;

    case ($uri == 'bug/ingenieur'):

        $controller = new engineerController();
        return $controller->index();
        break;
   
   
   
    case preg_match('#^ing/show/(\d+)$#', $uri, $matches):
         $id = $matches[1];
         $controller = new engineerController();
         return $controller->show($id);
         break;

         case preg_match('#^bug/update/(\d+)$#', $uri, $matches):

            $id = $matches[1];
   
            $controller = new bugController();
   
            return $controller->update($id);
   
            break;
   
   

    default:
    
    http_response_code(404);
    
    echo "<h1>Gestion d'incidents</h1><p>Page par défaut</p>";
    
}