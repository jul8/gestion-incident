<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class BugManager extends Manager
{

    public function find($id)
    {

        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug WHERE id = :id');
        $sth->bindParam(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        // Instanciation d'un bug
        $bug = new Bug();
        $bug->setId($result["id"]);
        $bug->setTitle($result["title"]);
        $bug->setDescription($result["description"]);
        $bug->setCreatedAt($result["createdAt"]);
        $bug->setClosedAt($result["closed"]);

        // Retour
        return $bug;
    }

    public function findAll()
    {

        // Récupération de tous les incidents en BDD
        // Connexion à la BDD
        $dbh = static::connectDb();

        // Requête
        $sth = $dbh->prepare('SELECT * FROM bug');
        $sth->execute();

        $bugs = [];

        while($result = $sth->fetch(\PDO::FETCH_ASSOC)) {
            // Instanciation d'un bug
            $bug = new Bug();
            $bug->setId($result["id"]);
            $bug->setTitle($result["title"]);
            $bug->setDescription($result["description"]);
            $bug->setCreatedAt($result["createdAt"]);
            $bug->setClosedAt($result["closed"]);
            array_push($bugs, $bug);
        }


        // Retour
        return $bugs;


    }

    public function add(Bug $bug){

        // Ajout d'un incident en BDD
        $dbh = static::connectDb();

        $req_insert = $dbh->prepare('INSERT INTO bug (title, description, createdAt) values (?,?,?)');
        $req_insert->execute([$bug->getTitle(), $bug->getDescription(), $bug->getCreatedAt()->format("Y-m-d H:i:s")]);

    }

    public function update(Bug $bug){

        // Update d'un incident en BDD
       $dbh = static::connectDb();

       $req = $dbh->prepare('UPDATE bug
                             SET closed = :clotureDate
                             WHERE id = '.$bug->getId()
                           );

       $req->execute(array(

              'clotureDate' => date("Y-m-d H:i:s")

       ));
    }








}
