<?php

namespace BugApp\Models;

use BugApp\Services\Manager;

class UserManager extends Manager {

     public function findByEmail($email){

         // Connexion à la BDD
         $dbh = static::connectDb();

         // Requête
         $sth = $dbh->prepare('SELECT * FROM user WHERE email = :email');
         $sth->bindParam(':email', $email, \PDO::PARAM_STR);
         $sth->execute();
         $result = $sth->fetch(\PDO::FETCH_ASSOC);

         // Instanciation d'un user

         if($result != null){

             switch($result['type']){

                 case 'engineer':
                     $user = new Engineer();
                     $user->setNom($result['nom']);
                     $user->setPassword($result['password']);
                     $user->setEmail($result['email']);
                     $user->setId($result['id']);
                     return $user;
                     break;

                 case 'recorder':
                     $user = new Recorder();
                     $user->setNom($result['nom']);
                     $user->setPassword($result['password']);
                     $user->setEmail($result['email']);
                     $user->setId($result['id']);
                     return $user;
                     break;

                 default:
                     die('You should not be there');

             }

         }else{

             return null;

         }

     }

     public function check($password, User $user){

         if($password == $user->getPassword()){
             return true;
         }else{
             return false;
         }

     }


}
