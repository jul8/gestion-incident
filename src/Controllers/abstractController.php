<?php

namespace BugApp\Controllers;

class abstractController
{
    public function render($templatePath, $parameters)
    {
        $templatePath = '../' . $templatePath . '.php';
        ob_start();
        $parameters;
        require($templatePath);
        return ob_get_clean();
    }
    public static function sendHttpResponse($content, $code = 200)
    {
        http_response_code($code);
        header('Content-Type: text/html');
        echo $content;
    }
}
