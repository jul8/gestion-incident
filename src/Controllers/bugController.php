<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);
        $type = $_SESSION['type'];

        if ($type == "recorder") {
            $content = $this->render('src/Views/Bug/show', ['bug' => $bug]);
        }

        if ($type == "engineer") {
            $content = $this->render('src/Views/ingenieur/show', ['bug' => $bug]);
        }

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {
        $manager = new BugManager();
        $bugs = $manager->findAll();
        $type = $_SESSION['type'];

        if ($type == "recorder") {
            $content = $this->render('src/Views/Bug/list', ['bugs' => $bugs]);
        }
        if ($type == "engineer") {
            $content = $this->render('src/Views/ingenieur/list', ['bugs' => $bugs]);
        }

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident
        if (isset($_POST['submit'])) {
            // Appel manager + insertion bdd
            $bug = new Bug();
            $bug->setTitle($_POST['title']);
            $bug->setDescription($_POST['description']);
            $bug->setCreatedAt($_POST['date']);

            $manager = new BugManager();
            $manager->add($bug);

            header('Location:'.PUBLIC_PATH.'bug');
        } else {
            $content = $this->render('src/Views/Bug/add', []);
            return $this->sendHttpResponse($content, 200);
        }
    }
    public function update($id)
    {

        // Update d'un incident
        $manager = new BugManager();
        $bug = $manager->find($id);

        if (isset($_POST['submit'])) {

            if (isset($_POST['cloture'])) {

                $manager->update($bug);
                header('Location:' . PUBLIC_PATH . 'Bug/show/' . $id);
            } else {
                header('Location:' . PUBLIC_PATH . 'Bug/update/' . $id);
            }
        } else {
            $content = $this->render('src/Views/ingenieur/update', ['bug' => $bug]);
            return $this->sendHttpResponse($content, 200);
        }
    }
}
