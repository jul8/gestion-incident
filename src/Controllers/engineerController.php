<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class engineerController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/ingenieur/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        $manager = new BugManager();
        $bugs = $manager->findAll();

        // TODO: liste des incidents

        $content = $this->render('src/Views/ingenieur/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }
}

    ?>