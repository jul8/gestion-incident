<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class recorderController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);

        // Template issu de la Vue

        $content = $this->render('src/Views/Bug/show', ['bug' => $bug]);

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {

        $manager = new BugManager();
        $bugs = $manager->findAll();

        // TODO: liste des incidents

        $content = $this->render('src/Views/Bug/list', ['bugs' => $bugs]);

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident

        // TODO: ajout d'incident (GET et POST)

        $manager = new BugManager();
        
        if(isset($_POST['submit'])) {
            $bug = new Bug();
            $bug->setTitle($_POST['title']);
            $bug->setDescription($_POST['description']);
            $bug->setCreatedAt($_POST['date'].' '.'00:00:00');
            $manager->add($bug);
            //header('Location: '.PUBLIC_PATH.'bug');
        }

        $content = $this->render('src/Views/Bug/add', []);

        return $this->sendHttpResponse($content, 200);
    }


}
