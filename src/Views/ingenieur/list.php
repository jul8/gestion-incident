<?php
$bugs = $parameters['bugs'];

?>
<!DOCTYPE html>
<html lang="en">
<?php
include (__DIR__.'./../include/header.php');
include (__DIR__.'./../include/nav.php');

?>
<body>
  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h1 class="header center orange-text">Listes des incidents</h1>
      
      <div class="row left">
        
        <form action="#">
          <p>
            <label>
              <input name="group1" type="radio" />
              <span>Afficher uniquement les incidents non-clôturés.</span>
            </label>
          </p>
          <p>
            <label>
              <input name="group1" type="radio" />
              <span>Afficher uniquement les incidents que je me suis assignés.</span>
            </label>
          </p>
       
        </form>
      </div>

      <div class="row center">
        <table class="responsive-table">
          <thead>
            <tr>
                <th>id</th>
                <th>Sujet</th>
                <th>Date</th>
                <th>Utilisateur</th>
                <th></th>
                <th>Ingégnieur</th>
                <th></th>
            </tr>
          </thead>
  
          <tbody>
            <tr>
            <?php
              foreach ($bugs as $bug) {
                echo '            
                <tr>
                <td>'.$bug->getId().'</td>
                <td>'.$bug->getTitle().'</td>
                <td>'.$bug->getCreatedAt()->format("Y-m-d").'</td>
                <td></td>
                <td><a href="'.PUBLIC_PATH.'bug/show/'.$bug->getId().'">Afficher</a></td>
                <td><a href="#">Assigner</a></td>
                <td><a href="#">Clôturer</a></td>
                </tr>';
              }
            ?>

            </tbody>
          </tbody>
        </table>
      </div>
      <br><br>

    </div>
  </div>



  </body>
</html>