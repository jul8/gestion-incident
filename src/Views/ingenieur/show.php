<?php

/** @var $bug \BugApp\Models\Bug */

$bug = $parameters['bug'];

?>
<!DOCTYPE html>
<html lang="en">

<?php
include (__DIR__.'./../include/header.php');
include (__DIR__.'./../include/nav.php');

?>
<body>
   
        <div class="container">
            <br><br>
            <h1 class="header center orange-text">Fiche descriptive d'incident</h1>
        
            <h5 class="header col s12 light"><a href="<?= PUBLIC_PATH; ?>bug"><i class="small material-icons">chevron_left</i></a>Listes des incidents</h5>

            <form>
                

                <div class="row">
                    <div class="input-field col s60">

                        <p>Nom de l'incident : <?=$bug->getTitle();?>
                        <a href="<?=PUBLIC_PATH?>bug/update/<?= $bug->getId() ?>"><i class="small material-icons">create</i></a></p>
                        

                    </div>
                    
                </div>
                <div class="row right">
                    <div class="input-field col s20">

                    <p><?php echo $bug->getCreatedAt()->format("d/m/Y");?></p>

                    </div>
                </div>




                <div class="row left">
                    <div class="input-field col s20">


                        <p>Description de l'incident :</p>

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s20">

                    <p><?=$bug->getDescription();?> </p>
                    </div>
                    
                </div>
            </form>
        </div>



            <script src="../mater/js/materialize.min.js"></script>



</body>

</html>