<?php
$bugs = $parameters['bugs'];

?>



<!DOCTYPE html>
<html lang="en">
<?php
include (__DIR__.'./../include/header.php');
include (__DIR__.'./../include/nav.php');

?>
<body>
<p>
      
   </p>
  
  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h1 class="header center orange-text">Listes des incidents</h1>
      
      <div class="row left">
        
        <h5 class="header col s12 light"><a href="<?= PUBLIC_PATH; ?>bug/add"><i class="small material-icons">add_circle</i></a>       Reporter un nouvel incident</h5>
      </div>

      <div class="row center">
        <table class="responsive-table">
          <thead>
            <tr>
                <th>id</th>
                <th>Sujet</th>
                <th>Date</th>
                <th>Date cloture</th>
                <th>Détails</th>
            </tr>
          </thead>
  
          <tbody>
            <?php
              foreach ($bugs as $bug) {
                echo '            
                <tr>
                <td>'.$bug->getId().'</td>
                <td>'.$bug->getTitle().'</td>
                <td>'.$bug->getCreatedAt()->format("Y-m-d").'</td>
                <td></td>
                <td><a href="'.PUBLIC_PATH.'bug/show/'.$bug->getId().'">Afficher</a></td>
                </tr>';
              }
            ?>
          </tbody>
        </table>
      </div>
      <br><br>

    </div>
  </div>



  </body>
</html>
